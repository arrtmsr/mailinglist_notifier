from django.utils import timezone
from celery import shared_task
from .models import MailingList, Message, Client
from .services import send_message

@shared_task
def process_active_mailings():
    active_mailings = MailingList.objects.filter(launch_date__lte=timezone.now(), end_date__gte=timezone.now())

    for mailing in active_mailings:
        clients = Client.objects.filter(mobile_operator_code=mailing.mobile_operator_code, tag=mailing.tag)

        for client in clients:
            message = Message.objects.create(mailing_list=mailing, client=client)
            send_message(message)

@shared_task          
def process_mailings():
        current_time = timezone.now()

        active_mailings = MailingList.objects.filter(launch_datetime__lte=current_time, end_datetime__gt=current_time)

        for mailing in active_mailings:
            filtered_clients = Client.objects.filter(
                mobile_operator_code=mailing.mobile_operator_code,
                tag=mailing.tag
            )

            for client in filtered_clients:
                message = Message.objects.create(
                    mailing_list=mailing,
                    client=client,
                    sending_status='PENDING',
                    creation_time=current_time
                )

                # Send message
                response = send_message(message)

                # Update sending status and statistics
                if response.status_code == 200:
                    message.sending_status = 'SENT'
                else:
                    message.sending_status = 'FAILED'

                message.save()
