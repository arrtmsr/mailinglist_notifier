from .models import MailingList, Message

def generate_general_statistics():
    mailing_count = MailingList.objects.count()
    sent_message_count = Message.objects.filter(sending_status='SENT').count()
    failed_message_count = Message.objects.filter(sending_status='FAILED').count()

    statistics = {
        'mailing_count': mailing_count,
        'sent_message_count': sent_message_count,
        'failed_message_count': failed_message_count,
    }

    return statistics

def generate_detailed_statistics(mailing_list_id):
    mailing_list = MailingList.objects.get(pk=mailing_list_id)
    sent_messages = Message.objects.filter(mailing_list=mailing_list, sending_status='SENT')

    statistics = {
        'mailing_list_id': mailing_list_id,
        'mailing_list_launch_date': mailing_list.launch_date,
        'mailing_list_end_date': mailing_list.end_date,
        'sent_message_count': sent_messages.count(),
        'sent_messages': [],
    }

    for message in sent_messages:
        message_data = {
            'message_id': message.id,
            'client_id': message.client.id,
            'client_phone_number': message.client.phone_number,
            'sending_status': message.sending_status,
            'statistics': message.statistics,
        }
        statistics['sent_messages'].append(message_data)

    return statistics
