from requests.exceptions import RequestException
from core.settings import ACCESS_TOKEN
from .models import Message, UnsentMessage
import requests

def send_message(message):
    # Send message to external API and collect statistics
    external_api_url = "https://probe.fbrq.cloud/v1/send/{msgId}".format(msgId=message.id)
    headers = {
        'Authorization': f'Bearer {ACCESS_TOKEN}',
        'Content-Type': 'application/json',
    }
    data = {
        'id': message.id,
        'phone': int(message.client.phone_number),
        'text': message.mailing_list.message,
    }
    
    # Set the maximum number of retries and timeout for each request
    max_retries = 3
    timeout = 10
    
    for retry in range(max_retries):
        try:
            response = requests.post(external_api_url, headers=headers, json=data, timeout=timeout)
            response.raise_for_status()
    
            if response.status_code == 200:
                message.sending_status = 'SENT'
                message.statistics = {
                    'response_code': response.status_code,
                    'response_data': response.json()
                }
            else:
                message.sending_status = 'FAILED'
                message.statistics = {
                    'response_code': response.status_code,
                    'response_data': response.text
                }
    
            message.save()
            break  # Break the loop if the request is successful
    
        except RequestException as e:
            # Log the error
            print(f"Error occurred during message sending: {e}")
    
            if retry < max_retries - 1:
                # Retry the request after a certain delay
                print(f"Retrying message sending (retry {retry + 1})...")
                continue
    
            # Request failed after max retries, handle the error
            print(f"Failed to send message after {max_retries} retries.")
            message.sending_status = 'FAILED'
            message.statistics = {
                'response_code': None,
                'response_data': str(e)
            }
            message.save()
            
            # Store unsent message locally for retry later
            store_unsent_message(message)
            break
    
    else:
        # Request failed after max retries, handle the error
        print(f"Failed to send message after {max_retries} retries.")
        message.sending_status = 'FAILED'
        message.statistics = {
            'response_code': None,
            'response_data': "Max retries exceeded"
        }
        message.save()
        
        # Store unsent message locally for retry later
        store_unsent_message(message)

def store_unsent_message(message):
    # Store the unsent message locally for retry later
    unsent_message = UnsentMessage(
        message_id=message.id,
        client_id=message.client.id,
        mailing_list_id=message.mailing_list.id
    )
    unsent_message.save()

def retry_unsent_messages():
    # Retry sending unsent messages when the external service is back online
    unsent_messages = UnsentMessage.objects.all()

    for unsent_message in unsent_messages:
        message = Message.objects.get(id=unsent_message.message_id)
        
        # Retry sending the message
        send_message(message)
        
        # Delete the unsent message after successful retry
        unsent_message.delete()
