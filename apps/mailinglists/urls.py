from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'clients', views.ClientViewSet)
router.register(r'mailing-lists', views.MailingListViewSet)
router.register(r'messages', views.MessageViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('statistics/general/', views.GeneralStatisticsAPIView.as_view(), name='general-statistics'),
    path('statistics/detailed/<int:mailing_list_id>/', views.DetailedStatisticsAPIView.as_view(), name='detailed-statistics'),
]