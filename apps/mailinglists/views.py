from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Client, MailingList, Message
from .serializers import ClientSerializer, MailingListSerializer, MessageSerializer
from .reports import generate_general_statistics, generate_detailed_statistics

class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

class MailingListViewSet(viewsets.ModelViewSet):
    queryset = MailingList.objects.all()
    serializer_class = MailingListSerializer

class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

class GeneralStatisticsAPIView(APIView):
    def get(self, request):
        statistics = generate_general_statistics()
        return Response(statistics)

class DetailedStatisticsAPIView(APIView):
    def get(self, request, mailing_list_id):
        statistics = generate_detailed_statistics(mailing_list_id)
        return Response(statistics)
        