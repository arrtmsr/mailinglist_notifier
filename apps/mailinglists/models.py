from django.db import models
from django.core.validators import RegexValidator

class Client(models.Model):
    phone_number_regex = RegexValidator(
        regex=r'^7\d{10}$',
        message='Номер телефона клиента должен быть в формате 7XXXXXXXXXX (X - цифра от 0 до 9)'
    )
    client_id = models.AutoField(primary_key=True)
    phone_number = models.CharField(max_length=11, unique=True, validators=[phone_number_regex])
    mobile_operator_code = models.CharField(max_length=10)
    tag = models.CharField(max_length=255)
    time_zone = models.CharField(max_length=255)

class MailingList(models.Model):
    mailing_id = models.AutoField(primary_key=True)
    launch_datetime = models.DateTimeField()
    message_text = models.TextField()
    mobile_operator_code = models.CharField(max_length=10)
    tag = models.CharField(max_length=255)
    end_datetime = models.DateTimeField()
    
    def get_matching_clients(self):
        """
        Get the clients that match the filter criteria of the mailing list
        """
        clients = Client.objects.all()

        if self.mobile_operator_code:
            clients = clients.filter(mobile_operator_code=self.mobile_operator_code)

        if self.tag:
            clients = clients.filter(tag=self.tag)

        return clients
    
class Message(models.Model):
    message_id = models.AutoField(primary_key=True)
    creation_datetime = models.DateTimeField(auto_now_add=True)
    sending_status = models.CharField(max_length=255)
    mailing_list = models.ForeignKey(MailingList, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

class UnsentMessage(models.Model):
    message = models.ForeignKey(Message, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    mailing_list = models.ForeignKey(MailingList, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"UnsentMessage: {self.message.id} - {self.client.id}"
