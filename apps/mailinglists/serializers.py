from rest_framework import serializers
from .models import Client, MailingList, Message
import re

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'
        
        def validate_phone_number(self, value):
            phone_number_regex = r'^7\d{10}$'
            if not re.match(phone_number_regex, value):
                raise serializers.ValidationError('Номер телефона клиента должен быть в формате 7XXXXXXXXXX (X - цифра от 0 до 9)')
            return value

class MailingListSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailingList
        fields = '__all__'
        
class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'