# Описание

В проекте используются библиотеки Django, DRF, Celery, Redis, Requests. В качестве дополнительного задания реализована документация в формате Swagger UI. 

### Запуск проекта
Для запуска проекта необходимы python, git, pipenv

1. Открыть терминал, склонировать проект и перейти в папку проекта
`git clone https://gitlab.com/arrtmsr/mailinglist_notifier.git` 
`cd mailinglist_notifier`
2. Запустить виртуальное окружение
`pipenv shell`
3. Установить необходимые библиотеки
`pipenv install -r requirements.txt`
4. Произвести миграцию
`./manage.py migrate`
5. Запустить тестовый сервер
`./manage.py runserver`

### Запуск проекта
По-умолчанию тестовый сервер должен открываться на 80-порту http://127.0.0.1:8000

Документация в формате OpenAPI находится http://127.0.0.1:8000/docs/

Все ендпройнты API формируются по http://127.0.0.1:8000/api/v1/<название_сервиса>/

Сервис содержит следующие ендпройнты:
- clients/
- mailing-lists/
- messages/
- statistics/general/
- statistics/detailed/<id_рассылки>/
